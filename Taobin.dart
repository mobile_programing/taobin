import 'dart:async';
import 'dart:io';

class Taobin {
  showWellcome() {
    print("---------WELLOCOME TO TAO BIN----------");
    print("---------คาเฟ่อัตโนมัติ 24 ชั่วโมง----------");
  }

  showTypeMenu() {
    print("1. กาแฟ");
    print("2. ชา");
    print("3. นมและโกโก้");
    print("4. โซดาและอื่นๆ");
    print("-------------------");
    print("กรุณาเลือกประเภทเครื่องดื่ม");
  }

  showMenuCoffee() {
    print("1. อเมริคาโน่");
    print("2. อเมริคาโน่ลิ้นจี่");
    print("3. อเมริคาโน่กัญชา");
    print("4. เอสเพรสโซ่");
    print("5. ดับเบิ้ลเอสเพรสโซ่");
    print("6. มอคค่า");
    print("7. ลาเต้");
    print("8. คาราเมลลาเต้");
    print("9. โคคุโตะลาเต้");
    print("10. คาปูชิโน่");
    print("11. มัคคิอาโต");
    print("12. กาแฟบราวน์ชูก้าร์");
    print("13. กาแฟมัชฉะลาเต้");
    print("14. กาแฟดำ");
    print("15. กาแฟชาไต้หวัน");
    print("16. กาแฟชาไทย");
    print("17. เดอร์ตี้");
    print("-------------------");
    print("กรุณาเลือกเมนูที่ต้องการ");
  }

  chooseCoffee(menu) {
    if (menu == 1) {
      print("-------------------");
      print("คุณได้เลือกเมนู อเมริคาโน่  "); //เริ่มต้น 40 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 2) {
      print("-------------------");
      print("คุณได้เลือกเมนู อเมริคาโน่ลิ้นจี่ "); //เริ่มต้น 50 บาท เย็นปั่น
      print("-------------------");
    } else if (menu == 3) {
      print("-------------------");
      print("คุณได้เลือกเมนู อเมริคาโน่กัญชา "); //เริ่มต้น 60 บาท เย็นปั่น
      print("-------------------");
    } else if (menu == 4) {
      print("-------------------");
      print("คุณได้เลือกเมนู เอสเพรสโซ่ "); //เริ่มต้น 40 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 5) {
      print("-------------------");
      print(
          "คุณได้เลือกเมนู ดับเบิ้ลเอสเพรสโซ่ "); //เริ่มต้น 50 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 6) {
      print("-------------------");
      print("คุณได้เลือกเมนู มอคค่า "); //เริ่มต้น 40 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 7) {
      print("-------------------");
      print("คุณได้เลือกเมนู ลาเต้ "); //เริ่มต้น 40 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 8) {
      print("-------------------");
      print("คุณได้เลือกเมนู คาราเมลลาเต้ "); //เริ่มต้น 50 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 9) {
      print("-------------------");
      print("คุณได้เลือกเมนู โคคุตะลาเต้ "); //เริ่มต้น 60 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 10) {
      print("-------------------");
      print("คุณได้เลือกเมนู คาปูชิโน่ "); //เริ่มต้น 40 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 11) {
      print("-------------------");
      print("คุณได้เลือกเมนู มัคคิอาโต "); //เริ่มต้น 50 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 12) {
      print("-------------------");
      print("คุณได้เลือกเมนู กาแฟบราวน์ชูก้า "); //เริ่มต้น 50 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 13) {
      print("-------------------");
      print("คุณได้เลือกเมนู กาแฟมัชฉะลาเต้ "); //เริ่มต้น 40 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 14) {
      print("-------------------");
      print("คุณได้เลือกเมนู กาแฟดำ "); //เริ่มต้น 40 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 15) {
      print("-------------------");
      print("คุณได้เลือกเมนู กาแฟชาไต้หวัน "); //เริ่มต้น 50 บาท ร้อนเย็น
      print("-------------------");
    } else if (menu == 16) {
      print("-------------------");
      print("คุณได้เลือกเมนู กาแฟชาไทย "); //เริ่มต้น 40 บาท ร้อนเย็นปั่น
      print("-------------------");
    } else if (menu == 17) {
      print("-------------------");
      print("คุณได้เลือกเมนู เดอร์ตี้ "); //เริ่มต้น 60 บาท ร้อนเย็นปั่น
      print("-------------------");
    }
  }

  showMenuTea() {
    print("1. ชาไทย");
    print("2. ชานมไต้หวัน");
    print("3. ชาบราวน์ชูการ์");
    print("4. ชาเขียวมะนาว");
    print("5. ชาเขียวญี่ปุ่ญเย็น");
    print("6. เก๊กฮวย");
    print("7. ชาโคคุโตะ");
    print("8. ชาลิ้นจี่");
    print("9. ชาเย็น");
    print("10. ชาสตรอเบอร์รี่");
    print("11. ชาบลูเบอร์รี่");
    print("-------------------");
    print("กรุณาเลือกเมนูที่ต้องการ");
  }

  chooseTea(menu) {
    if (menu == 1) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาไทย"); //เริ่มต้น 40 ร้อนเย็นปั่น
      print("ร้อน/เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 2) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชานมไต้หวัน"); //เริ่มต้น 40 เย็นปั่น
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 3) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาบราวน์ชูการ์"); //เริ่มต้น 50 เย็นปั่น
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 4) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาเขียวมะนาว"); //เริ่มต้น 40 เย็นปั่น
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 5) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาเขียวญี่ปุ่ญ"); //เริ่มต้น 40 ร้อนเย็นปั่น
      print("ร้อน/เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 6) {
      print("-------------------");
      print("คุณได้เลือกเมนู เก๊กฮวย"); //เริ่มต้น 40 เย็น
      print("เย็น");
      print("-------------------");
    }
    if (menu == 7) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาโคครุโตะ"); //เริ่มต้น 50 เย็นปั่น
      print("เย็น/ปั่น");
    }
    if (menu == 8) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาลิ้นจี่"); //เริ่มต้น 40 เย็นปั่น
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 9) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาเย็น"); //เริ่มต้น 40 เย็นปั่น
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 10) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาสตรอเอบร์รี่"); //เริ่มต้น 40 เย็นปั่น
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 11) {
      print("-------------------");
      print("คุณได้เลือกเมนู ชาบลูเบอร์รี่"); //เริ่มต้น 40 เย็นปั่น
      print("เย็น/ปั่น");
      print("-------------------");
    }
  }

  showMenumilk() {
    print("1. โอริโอ้ปั่นภูเขาไฟ"); // 60
    print("2. นมสตรอว์เบอร์รี่"); //50
    print("3. โกโก้"); //40
    print("4. คาราเมลโกโก้"); //50
    print("5. นมโคคุโตะ"); //50
    print("6. นมร้อน"); //35
    print("7. นมชมพู"); //40
    print("-------------------");
    print("กรุณาเลือกเมนูที่ต้องการ");
  }

  chooseMilk(menu) {
    if (menu == 1) {
      print("-------------------");
      print("คุณได้เลือกเมนู โอริโอ้ปั่นภูเขาไฟ");
      print("ปั่น");
      print("-------------------");
    }
    if (menu == 2) {
      print("-------------------");
      print("คุณได้เลือกเมนู นมสตรอเบอร์รี่");
      print("เย็น");
      print("-------------------");
    }
    if (menu == 3) {
      print("-------------------");
      print("คุณได้เลือกเมนู โกโก้");
      print("ร้อน/เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 4) {
      print("-------------------");
      print("คุณได้เลือกเมนู คาราเมลโกโก้");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 5) {
      print("-------------------");
      print("คุณได้เลือกเมนู นมโคคุโตะ");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 6) {
      print("-------------------");
      print("คุณได้เลือกเมนู นมร้อน");
      print("ร้อน");
      print("-------------------");
    }
    if (menu == 7) {
      print("-------------------");
      print("คุณได้เลือกเมนู นมชมพู");
      print("เย็น/ปั่น");
      print("-------------------");
    }
  }

  showMenuSoda() {
    print("1. เป๊ปซี่"); //40.
    print("2. น้ำมะนาวโซดา"); //40.
    print("3. บ๊วยโซดา"); //40
    print("4. ขิงโซดา");
    print("5. น้ำลิ้นจี่โซดา");
    print("6. น้ำสตรอเบอร์โซดารี่");
    print("7. น้ำบลูเบอร์รี่โซดา");
    print("8. น้ำแดงสละโซดา");
    print("9. น้ำแดงสละมะนาวโซดา");
    print("10. น้ำกัญชาโซดา");
    print("11. น้ำมะนาว");
    print("12. น้ำลิ้นจี่");
    print("-------------------");
    print("กรุณาเลือกเมนูที่ต้องการ");
  }

  chooseSoda(menu) {
    if (menu == 1) {
      print("-------------------");
      print("คุณได้เลือกเมนู เป็ปซี่");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 2) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำมะนาวโซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 3) {
      print("-------------------");
      print("คุณได้เลือกเมนู บ๊วยโซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 4) {
      print("-------------------");
      print("คุณได้เลือกเมนู ขิงโซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 5) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำลิ้นจี่โซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 6) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำสตรอเบอร์รี่โซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 7) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำบลูเบอร์รี่โซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 8) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำแดงสละโซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 9) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำแดงสละมะนาวโซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 10) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำกัญชาโซดา");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 11) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำมะนาว");
      print("เย็น/ปั่น");
      print("-------------------");
    }
    if (menu == 12) {
      print("-------------------");
      print("คุณได้เลือกเมนู น้ำลิ้นจี่");
      print("เย็น/ปั่น");
      print("-------------------");
    }
  }

  showSugarLevel() {
    print("1. หวานน้อยมาก");
    print("2. หวานน้อย");
    print("3. หวานปกติ");
    print("4. หวานมาก");
    print("5. หว๊านหวาน");
    print("-------------------");
    print("กรุณาเลือกความหวาน");
  }

  chooseSugar(sugar) {
    if (sugar == 1) {
      print("-------------------");
      print("หวานน้อยมาก");
      print("-------------------");
    }
    if (sugar == 2) {
      print("-------------------");
      print("หวานน้อย");
      print("-------------------");
    }
    if (sugar == 3) {
      print("-------------------");
      print("หวานปกติ");
      print("-------------------");
    }
    if (sugar == 4) {
      print("-------------------");
      print("หวานมาก");
      print("-------------------");
    }
    if (sugar == 5) {
      print("-------------------");
      print("หว๊านหวาน");
      print("-------------------");
    }
  }

  showType() {
    print("1. ร้อน");
    print("2. เย็น(+10)");
    print("3. ปั่น(+10)");
    print("-------------------");
    print("กรุณาเลือกชนิด");
  }

  showTwoType() {
    print("1. เย็น(+10)");
    print("2. ปั่น(+10)");
    print("-------------------");
    print("กรุณาเลือกชนิด");
  }

  showOneType() {
    print("1. เย็น(+10)");
    print("-------------------");
    print("กรุณาเลือกชนิด");
  }

  chooseType(hcf) {
    if (hcf == 1) {
      print("-------------------");
      print("ร้อน");
      print("-------------------");
    }
    if (hcf == 2) {
      print("-------------------");
      print("เย็น");
      print("-------------------");
    }
    if (hcf == 3) {
      print("-------------------");
      print("ปั่น");
      print("-------------------");
    }
  }

  showPayment() {
    print("1. เงินสด");
    print("2. QR");
    print("-------------------");
    print("เลือกวิธีชำระเงิน");
  }

  member() {
    print("ต้องการสะสมแต้มไหม");
    print("1. ต้องการ");
    print("2. ไม่ต้องการ");
  }
}

void main(List<String> args) {
  Taobin taobin1 = Taobin();

  taobin1.showWellcome();
  //เลือกประเภทเครื่องดื่ม
  taobin1.showTypeMenu();
  int type = int.parse(stdin.readLineSync()!);

  if ((type == 1) || (type == 2) || (type == 3) || (type == 4)) {
    if (type == 1) {
      //เลือกเมนู
      taobin1.showMenuCoffee();
      var menu = int.parse(stdin.readLineSync()!);
      taobin1.chooseCoffee(menu);

      //เลือกชนิด ร้อน เย็น ปั่น
      taobin1.showType();
      var hcf = int.parse(stdin.readLineSync()!);
      taobin1.chooseType(hcf);

      //เลือกระดับความหวาน
      taobin1.showSugarLevel();
      var sugar = int.parse(stdin.readLineSync()!);
      taobin1.chooseSugar(sugar);

      //ยอดเงินรวม
      if (menu == 1 ||
          menu == 4 ||
          menu == 6 ||
          menu == 7 ||
          menu == 10 ||
          menu == 13 ||
          menu == 14 ||
          menu == 16) {
        if (hcf == 1) {
          print("ยอดรวมทั้งหมด 40 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          if (payment == 1) {
            print("กรุณาใส่จำนวนเงิน");
            var money = int.parse(stdin.readLineSync()!);
            if (money >= 40) {
              double sum = money - 40;
              print("เงินทอน $sum บาท");
            } else {
              print("เกิดข้อผิดพลาด");
            }
          } else if (payment == 2) {
            print("สแกน QRcodeได้เลย");
            sleep(Duration(seconds: 4));
            print("กรุณารอสักครู่");
            sleep(Duration(seconds: 7));
            print("ชำระเงินเรียบร้อยแล้ว");
          }
        } else if (hcf == 2) {
          print("ยอดรวมทั้งหมด 50 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          if (payment == 1) {
            print("กรุณาใส่จำนวนเงิน");
            var money = int.parse(stdin.readLineSync()!);
            if (money >= 50) {
              double sum = money - 50;
              print("เงินทอน $sum บาท");
            } else {
              print("เกิดข้อผิดพลาด");
            }
          } else if (payment == 2) {
            print("สแกน QRcodeได้เลย");
            sleep(Duration(seconds: 4));
            print("กรุณารอสักครู่");
            sleep(Duration(seconds: 7));
            print("ชำระเงินเรียบร้อยแล้ว");
          }
        } else if (hcf == 3) {
          print("ยอดรวมทั้งหมด 60 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 60) {
                double sum = money - 60;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      } else if (menu == 2 ||
          menu == 5 ||
          menu == 8 ||
          menu == 11 ||
          menu == 12 ||
          menu == 15) {
        if (hcf == 1) {
          print("ยอดรวมทั้งหมด 50 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          if (payment == 1) {
            print("กรุณาใส่จำนวนเงิน");
            var money = int.parse(stdin.readLineSync()!);
            if (money >= 50) {
              double sum = money - 50;
              print("เงินทอน $sum บาท");
            } else {
              print("เกิดข้อผิดพลาด");
            }
          } else if (payment == 2) {
            print("สแกน QRcodeได้เลย");
            sleep(Duration(seconds: 4));
            print("กรุณารอสักครู่");
            sleep(Duration(seconds: 7));
            print("ชำระเงินเรียบร้อยแล้ว");
          }
        } else if (hcf == 2) {
          print("ยอดรวมทั้งหมด 60 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          if (payment == 1) {
            print("กรุณาใส่จำนวนเงิน");
            var money = int.parse(stdin.readLineSync()!);
            if (money >= 60) {
              double sum = money - 60;
              print("เงินทอน $sum บาท");
            } else {
              print("เกิดข้อผิดพลาด");
            }
          } else if (payment == 2) {
            print("สแกน QRcodeได้เลย");
            sleep(Duration(seconds: 4));
            print("กรุณารอสักครู่");
            sleep(Duration(seconds: 7));
            print("ชำระเงินเรียบร้อยแล้ว");
          }
        } else if (hcf == 3) {
          print("ยอดรวมทั้งหมด 70 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 70) {
                double sum = money - 70;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      } else if (menu == 3 || menu == 7 || menu == 17) {
        if (hcf == 1) {
          print("ยอดรวมทั้งหมด 40 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          if (payment == 1) {
            print("กรุณาใส่จำนวนเงิน");
            var money = int.parse(stdin.readLineSync()!);
            if (money >= 40) {
              double sum = money - 40;
              print("เงินทอน $sum บาท");
            } else {
              print("เกิดข้อผิดพลาด");
            }
          } else if (payment == 2) {
            print("สแกน QRcodeได้เลย");
            sleep(Duration(seconds: 4));
            print("กรุณารอสักครู่");
            sleep(Duration(seconds: 7));
            print("ชำระเงินเรียบร้อยแล้ว");
          }
        } else if (hcf == 2) {
          print("ยอดรวมทั้งหมด 50 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          if (payment == 1) {
            print("กรุณาใส่จำนวนเงิน");
            var money = int.parse(stdin.readLineSync()!);
            if (money >= 50) {
              double sum = money - 50;
              print("เงินทอน $sum บาท");
            } else {
              print("เกิดข้อผิดพลาด");
            }
          } else if (payment == 2) {
            print("สแกน QRcodeได้เลย");
            sleep(Duration(seconds: 4));
            print("กรุณารอสักครู่");
            sleep(Duration(seconds: 7));
            print("ชำระเงินเรียบร้อยแล้ว");
          }
        } else if (hcf == 3) {
          print("ยอดรวมทั้งหมด 60 บาท ");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 60) {
                double sum = money - 60;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      }
    } else if (type == 2) {
      //เลือกเมนู
      taobin1.showMenuTea();
      var menu = int.parse(stdin.readLineSync()!);
      taobin1.chooseTea(menu);

      //เลือกชนิด ร้อน เย็น ปั่น
      taobin1.showType();
      var hcf = int.parse(stdin.readLineSync()!);
      taobin1.chooseType(hcf);

      //เลือกระดับความหวาน
      taobin1.showSugarLevel();
      var sugar = int.parse(stdin.readLineSync()!);
      taobin1.chooseSugar(sugar);
      //คำนวนเงิน
      if (menu == 6) {
        print("ยอดรวมทั้งหมด 35 บาท");
        taobin1.showPayment();
        var payment = int.parse(stdin.readLineSync()!);
        if (hcf == 2) {
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 35) {
                double sum = money - 35;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      } else if (menu == 1 ||
          menu == 5 ||
          menu == 2 ||
          menu == 4 ||
          menu == 8 ||
          menu == 9 ||
          menu == 10 ||
          menu == 11) {
        if (hcf == 1) {
          print("ยอดรวมทั้งหมด 40 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 40) {
                double sum = money - 40;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 2) {
          print("ยอดรวมทั้งหมด 50 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 50) {
                double sum = money - 50;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 3) {
          print("ยอดรวมทั้งหมด 60 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 60) {
                double sum = money - 60;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      } else if (menu == 3 || menu == 7) {
        if (hcf == 1) {
          print("ยอดรวมทั้งหมด 50 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 50) {
                double sum = money - 50;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 2) {
          print("ยอดรวมทั้งหมด 60 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 60) {
                double sum = money - 60;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 3) {
          print("ยอดรวมทั้งหมด 70 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 70) {
                double sum = money - 70;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      }
    } else if (type == 3) {
      //เลือกเมนู
      taobin1.showMenumilk();
      var menu = int.parse(stdin.readLineSync()!);
      taobin1.chooseMilk(menu);

      //เลือกชนิด ร้อน เย็น ปั่น
      taobin1.showType();
      var hcf = int.parse(stdin.readLineSync()!);
      taobin1.chooseType(hcf);

      //เลือกระดับความหวาน
      taobin1.showSugarLevel();
      var sugar = int.parse(stdin.readLineSync()!);
      taobin1.chooseSugar(sugar);

      //คิดเงิน
      if (menu == 3 || menu == 6 || menu == 7) {
        if (hcf == 1) {
          print("ยอดรวมทั้งหมด 40 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 40) {
                double sum = money - 40;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 2) {
          print("ยอดรวมทั้งหมด 50 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 50) {
                double sum = money - 50;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 3) {
          print("ยอดรวมทั้งหมด 60 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 60) {
                double sum = money - 60;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      } else if (menu == 2 || menu == 4 || menu == 5) {
        if (hcf == 1) {
          print("ยอดรวมทั้งหมด 50 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 50) {
                double sum = money - 50;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 2) {
          print("ยอดรวมทั้งหมด 60 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 60) {
                double sum = money - 60;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 3) {
          print("ยอดรวมทั้งหมด 70 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 70) {
                double sum = money - 70;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      } else if (menu == 1) {
        if (hcf == 3) {
          print("ยอดรวมทั้งหมด 70 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 70) {
                double sum = money - 70;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      }
    } else if (type == 4) {
      //เลือกเมนู
      taobin1.showMenuSoda();
      var menu = int.parse(stdin.readLineSync()!);
      taobin1.chooseSoda(menu);

      //เลือกชนิด ร้อน เย็น ปั่น
      taobin1.showType();
      var hcf = int.parse(stdin.readLineSync()!);
      taobin1.chooseType(hcf);

      //เลือกระดับความหวาน
      taobin1.showSugarLevel();
      var sugar = int.parse(stdin.readLineSync()!);
      taobin1.chooseSugar(sugar);
      //คำนวนเงิน
      if (menu == 1 ||
          menu == 2 ||
          menu == 3 ||
          menu == 4 ||
          menu == 5 ||
          menu == 6 ||
          menu == 7 ||
          menu == 8 ||
          menu == 9 ||
          menu == 10 ||
          menu == 11 ||
          menu == 12) {
        if (hcf == 2) {
          print("ยอดรวมทั้งหมด 40 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 40) {
                double sum = money - 40;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        } else if (hcf == 3) {
          print("ยอดรวมทั้งหมด 50 บาท");
          taobin1.showPayment();
          var payment = int.parse(stdin.readLineSync()!);
          while (true) {
            if (payment == 1) {
              print("กรุณาใส่จำนวนเงิน");
              var money = int.parse(stdin.readLineSync()!);
              if (money >= 50) {
                double sum = money - 50;
                print("เงินทอน $sum บาท");
                break;
              } else {
                print("เกิดข้อผิดพลาด");
              }
            } else if (payment == 2) {
              print("สแกน QRcodeได้เลย");
              sleep(Duration(seconds: 4));
              print("กรุณารอสักครู่");
              sleep(Duration(seconds: 7));
              print("ชำระเงินเรียบร้อยแล้ว");
              break;
            }
          }
        }
      }
    }
  }
  taobin1.member();
  var member = int.parse(stdin.readLineSync()!);
  if (member == 1) {
    print("กรุณากรอกเบอร์โทรศัพท์");
    var numberPhone = int.parse(stdin.readLineSync()!);
    print("กรุณารอสักครู่");
    sleep(Duration(seconds: 4));
    print("คุณได้รับ 2 แต้ม");
    sleep(Duration(seconds: 2));
    print("พร้อมเสิร์ฟในอีกสักครู่");
    sleep(Duration(seconds: 7));
    print("เรียบร้อย");
    sleep(Duration(seconds: 2));
    print("ขอบคุณค่ะ");
  } else if (member == 2) {
    print("พร้อมเสิร์ฟในอีกสักครู่");
    sleep(Duration(seconds: 7));
    print("เรียบร้อย");
    sleep(Duration(seconds: 2));
    print("ขอบคุณค่ะ");
  }
}
